# Algorithms & Data Structures
# SI3 - Polytech Nice-Sophia - Edition 2018
# Python 3.6
# by Marc Gaetano
from typing import List
from Lab5 import BinaryHeap
import slowinteger
import random
import time

def selectionSort(L:List):
    '''
Selection Sort
    '''
    for i in range(len(L)):
        min = i
        for j in range(i,len(L)):
            if L[j]<L[min]:min=j
        L[i],L[min] = L[min],L[i]

def insertionSort(L:List):
    '''
Insertion Sort
    '''
    for i in range(len(L)):
        j = i
        while L[j-1]>=L[i] and j>0:
            j-=1
        L.insert(j,L.pop(i))



def mergeSort(L:List):
    '''
Mergesort
    '''
    
    def merge(fromList,toList,low,middle,high):
        j,i = low,middle
        for n in range(low,high):
            if i>=high or (j<middle and fromList[j]<fromList[i]):
                toList[n]=fromList[j]
                j+=1
            else:
                toList[n] = fromList[i]
                i+=1


    
    def transfer(fromList,toList,low,high):
        for i in range(low,high):toList[i]=fromList[i]

    def mergeAux(alist,helper,low,high):
        if high-low>1:
            m = (low+high)//2
            mergeAux(alist,helper,low,m)
            mergeAux(alist,helper,m,high)
            merge(alist,helper,low,m,high)
            transfer(helper,alist,low,high)
    mergeAux(L,L[:],0,len(L))

    
def quickSort(L,cutoff=10):
    '''
Quicksort
    '''
    
    def insertionSort(L,left,right):
        '''
    insertion sort of L[left:right]
        '''
        for i in range(left,right):
            j = i
            while L[j - 1] >= L[i] and j > 0:
                j -= 1
            L.insert(j, L.pop(i))


    def partition(L:List,i,j):
        '''
    Do the partition of L[i:j] and return the index of pivot
        '''
        index = random.randint(i,j-1)


        L[i],L[index] = L[index],L[i]
        index = i+1
        for e in range(index,j):

            if L[e]<L[i]:
                L[e], L[index] = L[index], L[e]
                index+=1
        index-=1
        L[i], L[index] = L[index], L[i]
        return index

    def quickAux(L,i,j):
        '''
    Recursively quicksort L[i:j]
        '''
        if  abs(j-i)<=cutoff:insertionSort(L,i,j)
        else :
            pivot = partition(L,i,j)
            quickAux(L,i,pivot)
            quickAux(L,pivot+1,j)
    
    quickAux(L,0,len(L))

def heapSort(L):
    '''
Heapsort
    '''
    heap = BinaryHeap(L)
    heap.buildHeap()
    for i in range(len(L)):
        L[i] = heap.pop()



###
### DO NOT CHANGE THE CODE BELOW
###

def isSorted(L):
    for i in range(1,len(L)):
        if L[i-1] > L[i]:
            return False
    return True

if __name__ == '__main__':
    if False:
        L = [1, 0, 3, 1, 5, 4, 3]
        heapSort(L)
        print(L)
    else:
        fun = [ selectionSort, insertionSort, heapSort, quickSort, mergeSort ]
        l = []
        while True:
            choice = input("exit(0) generate a new list(1) selection sort(2) insertion sort(3) heapsort(4) quicksort(5) mergesort(6): ")
            if choice == "0":
                break
            if choice == "1":
                m = int(input("\nenter maximum value in the list: ").strip())
                n = int(input("enter the number of values in the list: ").strip())
                l.clear()
                for i in range(n+1):
                    l.append(slowinteger.slowinteger(1,m))
                print(l)
                print()
            else:
                copy = l[:]
                f = fun[int(choice)-2]
                print(f.__doc__)
                print("random:",copy)
                start = time.time()
                f(copy)
                stop = time.time()
                print("sorted:",copy)
                print("result is correct!" if isSorted(copy) else "result is NOT correct!")
                print("computation time: {:1.3f}s\n".format(stop - start))
