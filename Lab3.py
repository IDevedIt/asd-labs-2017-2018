# Algorithms & Data Structures
# SI3 - Polytech Nice-Sophia - Edition 2018
# Python 3.6
# by Christophe Papazian

'''
IMPORTANT NOTICE
Don't use any import
You must complete all methods and functions with 'pass'
'''


class Tree:
    '''
    Class for Binary Tree

    Attributes:
        data                 : what the node contains
        left  (Tree or None) : left son
        right (Tree or None) : right son
    '''

    def __init__(self, data=None, left=None, right=None):
        self.data, self.left, self.right = data, left, right

    @property
    def sons(self):
        return (self.left, self.right)

    def is_leaf(self):
        return (self.left is None) and (self.right is None)

    def __getitem__(self, i):
        '''Accessors'''
        return self.right if i else self.left

    def print(self, prev="", dataprev=""):
        '''
        print the tree in a readable format. Use unicode box drawing characters.
        '''
        print(dataprev + self.data)
        if self.left is not None or self.right is not None:
            nprev = prev + '\u2503 '
            if self.left is not None:
                self.left.print(nprev, prev + '\u2523\u2578')
            else:
                print(prev + '\u2523\u2578')
            nprev = prev + '  '
            if self.right is not None:
                self.right.print(nprev, prev + '\u2517\u2578')
            else:
                print(prev + '\u2517\u2578')


def height(tree):
    '''
    compute the height of a tree
    '''
    return -1 if tree is None else 1 + max(height(son) for son in tree.sons)


def lowness(tree: Tree):
    '''
    compute the lowness of a tree
    '''
    if not tree: return -1
    if tree.is_leaf(): return 0
    return min(map(lambda t: lowness(t), filter(lambda t: t, tree.sons))) + 1


def size(tree: Tree):
    '''
    compute the size of a tree (number of nodes)
    '''
    if not tree: return 0
    return 1 + sum(map(size, tree.sons))


def leaves(tree: Tree):
    '''
    compute the number of leaves of a tree
    '''
    if not tree: return 0
    if tree.is_leaf(): return 1
    return sum(map(leaves, tree.sons))


def isomorphic(t1: Tree, t2: Tree):
    '''
    return a bool
    True if the two trees are isomorphic (same structure, data is irrelevant)
    '''
    if not t1 or not t2: return t1 == t2
    return isomorphic(t1.right, t2.right) and isomorphic(t1.left, t2.left)


def balanced1(tree: Tree):
    '''
    return a bool
    True if the tree is balanced. Use height and is in O(n**2)
    '''
    if not tree or tree.is_leaf(): return True
    return abs(height(tree.right) - height(tree.left)) <= 1 and balanced1(tree.left) and balanced1(tree.right)


def balanced2(tree):
    '''
    return a bool
    True if the tree is balanced. Don't use height and is in O(n)
    '''

    def explore(tree):
        if not tree: return -1
        left, right = explore(tree.left), explore(tree.right)

        if left is None or right is None or abs(left - right) > 1: return None
        return max(left, right) + 1

    return explore(tree) is not None


def shapely1(tree: Tree):
    '''
    return a bool
    True if the tree is shapely. Use height and lowness and is in O(n**2)
    '''
    if tree.is_leaf(): return True

    return all(map(lambda t: height(t) <= lowness(t) * 2 and shapely1(t), filter(lambda t: t, tree.sons)))and height(tree)<=lowness(tree)*2


def shapely2(tree: Tree):
    '''
    return a bool
    True if the tree is shapely. Don't use height and lowness and is in O(n)
    '''

    def explore(tree: Tree):
        if not tree: return (-1, -1)
        left, right = explore(tree.left), explore(tree.right)
        if left is None or right is None: return None
        treeHeight = max(left[0], right[0]) + 1
        treeLowness = min(left[1], right[1])
        if treeLowness < 0: treeLowness = max(left[1], right[1])
        treeLowness += 1
        if not (treeHeight <= treeLowness * 2): return None

        return (treeHeight, treeLowness)

    return explore(tree) is not None


class ExpressionTree(Tree):
    @property
    def operator(self):
        return {
            "+": lambda x, y: x + y,
            "-": lambda x, y: x - y,
            "*": lambda x, y: x * y,
            "^": pow,
            "/": lambda x, y: x / y
        }[self.data]

    def eval(self):
        '''
        return an int as a result of evaluating the expression in the tree
        '''
        if self.is_leaf(): return int(self.data)
        return int(self.operator(self.left.eval(), self.right.eval()))


class GenealogyTree(Tree):
    def ancestors(self, n, male=False):
        '''
        returns the list of ancestors of the person at the root of the genealogy tree at a given level
        if male is True, only return the males at that level
        '''

        if n > 1: return (self.left.ancestors(n - 1, male) if self.left else[]) + (self.right.ancestors(n - 1, male)if self.right else [])
        result = ([self.left.data] if self.left else []) + ([self.right.data] if (self.right and not male) else [])
        return result


####
#### Don't change anything under this comment, the class test is here to help you evaluate your work.
#### You will need the file big-file.txt in the same directory for the test on pairing.
####

import time, math


def timerfunc(N):
    def tf(func):
        """
        A timer decorator
        """

        def function_timer(*args, **kwargs):
            """
            A nested function for timing other functions
            """
            timeres = []
            for i in range(N):
                start = time.time()
                value = func(*args, **kwargs)
                end = time.time()
                timeres.append(end - start)
            timeres.remove(min(timeres))
            timeres.remove(max(timeres))
            runtime = math.fsum(timeres) / len(timeres)
            print("\nThe runtime for %s took %g milliseconds to complete" % (func.__name__, runtime * 1000))
            return value

        return function_timer

    return tf


import unittest


def build_tree(s, build=Tree):
    l = s.split()

    def aux(i):
        if i >= len(l) or l[i] == '!': return i + 1, None
        tag = l[i]
        i, left = aux(i + 1)
        i, right = aux(i)
        return i, build(tag, left, right)

    return aux(0)[1]


def complete_tree(n):
    if n >= 0:
        s = complete_tree(n - 1)
        return Tree('T', s, s)


class TestLab2(unittest.TestCase):

    def testA_lowness(self):
        self.assertEqual(lowness(build_tree("A")), 0)
        self.assertEqual(lowness(build_tree("A B C")), 2)
        self.assertEqual(lowness(build_tree("A B ! ! C")), 1)
        self.assertEqual(lowness(build_tree("A B C ! ! ! D E ! ! F")), 2)

    def testB_size(self):
        self.assertEqual(size(build_tree("A")), 1)
        self.assertEqual(size(build_tree("A B C")), 3)
        self.assertEqual(size(build_tree("A B ! ! C")), 3)
        self.assertEqual(size(build_tree("A B C ! ! ! D E ! ! F")), 6)

    def testC_leaves(self):
        self.assertEqual(leaves(build_tree("A")), 1)
        self.assertEqual(leaves(build_tree("A B C")), 1)
        self.assertEqual(leaves(build_tree("A B ! ! C")), 2)
        self.assertEqual(leaves(build_tree("A B C ! ! ! D E ! ! F")), 3)

    def testD_isomorphic(self):
        self.assertTrue(isomorphic(build_tree("A"), build_tree("B")))
        self.assertFalse(isomorphic(build_tree("A ! B"), build_tree("B A")))

    CT10 = complete_tree(10)
    CT8 = complete_tree(8)
    NCT = Tree('T', Tree('L', CT8, None), Tree('R', None, CT8))
    NST = build_tree("T L L ! ! ! R")

    @timerfunc(15)
    def testE_balanced1(self):
        self.assertTrue(balanced1(self.CT10))
        self.assertFalse(balanced1(self.NCT))
        self.assertTrue(balanced1(self.NST))
        self.assertFalse(balanced1(build_tree("A B C ! ! D")))
        self.assertFalse(balanced1(build_tree("A B C D ! ! ! E ! ! F")))

    @timerfunc(15)
    def testF_balanced2(self):
        self.assertTrue(balanced2(self.CT10))
        self.assertFalse(balanced2(self.NCT))
        self.assertTrue(balanced2(self.NST))

    @timerfunc(15)
    def testG_shapely1(self):
        self.assertTrue(shapely1(self.CT10))
        self.assertTrue(shapely1(self.NCT))
        self.assertTrue(shapely1(self.NST))
        self.assertFalse(shapely1(build_tree("A B C D ! ! ! E ! ! F")))

    @timerfunc(15)
    def testH_shapely2(self):
        self.assertTrue(shapely2(self.CT10))
        self.assertTrue(shapely2(self.NCT))
        self.assertTrue(shapely2(self.NST))

    def testI_ExpressionTree(self):
        self.assertEqual(build_tree("* + 3 ! ! 7 ! ! - 12 ! ! ^ 3 ! ! 2", ExpressionTree).eval(), 30)
        self.assertEqual(build_tree("* + / 13 ! ! 2 ! ! 7 ! ! - 12 ! ! ^ 3 ! ! 2", ExpressionTree).eval(), 39)

    GT = build_tree(
        "Edward David Carl ! Barbara Anthony ! ! Anna ! ! ! Dorothy Craig Bruce Allan ! ! Amanda ! ! ! Carol Brian Andrew ! ! Ann ! ! Brenda Albert ! ! Alice",
        GenealogyTree)

    def testJ_GenealogyTree(self):
        self.assertEqual(self.GT.ancestors(2), ['Carl', 'Craig', 'Carol'])
        self.assertEqual(self.GT.ancestors(2, male=True), ['Carl', 'Craig'])
        self.assertEqual(self.GT.ancestors(4),
                         ['Anthony', 'Anna', 'Allan', 'Amanda', 'Andrew', 'Ann', 'Albert', 'Alice'])
        self.assertEqual(self.GT.ancestors(4, male=True), ['Anthony', 'Allan', 'Andrew', 'Albert'])


import sys

if __name__ == "__main__" and sys.flags.interactive == 0:
    unittest.main()
