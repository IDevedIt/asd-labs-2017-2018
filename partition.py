# Algorithms & Data Structures
# SI3 - Polytech Nice-Sophia - Edition 2018
# Python 3.6
# by Marc Gaetano

class Partition:
    """
  Union-find structure for maintaining disjoint sets
    """

    # ------------------------- nested Node class -------------------------
    class Node:
        __slots__ = '_partition', '_value', '_size', '_parent'

        def __init__(self, partition, e):
            """
          Create a new node that is the leader of its group
            """
            self._partition = partition  # reference to Partition instance
            self._value = e
            self._size = 1
            self._parent = self  # to detect a group leader

        def value(self):
            """
          Return value stored at this node
            """
            return self._value

        def is_leader(self):
            return self._parent == self

        @property
        def parent(self):
            return self._parent

    # ------------------------- private utility -------------------------
    def _validate(self, p):
        """
      Check if p is a Node belonging to this container.
      Raise an exception otherwise
        """
        if not isinstance(p, self.Node):
            raise TypeError('p must be proper Node type')
        if p._partition is not self:
            raise ValueError('p does not belong to this container')

    # ------------------------- public Partition methods -------------------------
    def __init__(self):
        self._count = 0

    def __len__(self):
        return self._count

    def create_node(self, e):
        """
      Creates a new node containing value e, and returns it
        """
        self._count += 1
        return self.Node(self, e)

    def find(self, p: Node):
        """
      Returns the node leader of the group containing node p.
      Performs path compression on the path from p to the leader
        """
        path = []
        while not p.is_leader():
            path.append(p)
            p = p.parent
        for n in path:
            n._parent = p

        return p

    def union(self, p:Node, q:Node):
        """
      Merges the groups of leaders p and q (p and q must be distinct)
        """

        if(q._size>p._size):
            p._parent = q
            q._size +=p._size
        else:
            q._parent = p
            p._size +=q._size
        self._count-=1

if __name__ == '__main__':
    p = Partition()
    lst = [p.create_node(i) for i in range(128)]
    for d in range(7):
        for i in range(0, 128, 1 << (d + 1)):
            p.union(p.find(lst[i]), p.find(lst[i + (1 << d)]))
    assert (p.find(lst[127])._size == 128)
    assert (p._count == 1)
    print('Main root is %d' % p.find(lst[0]).value())
    print('OK')